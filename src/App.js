import './App.css';
import ExGlassMapping from './ExGlassMapping/ExGlassMapping';

function App() {
  return (
    <div className="App">
      {/* Content here */}
      <ExGlassMapping></ExGlassMapping>
    </div>
  );
}

export default App;
