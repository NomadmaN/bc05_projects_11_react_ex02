import React, { Component } from 'react'
import { dataGlass } from './dataGlass'
import GlassDetail from './GlassDetail'
import GlassList from './GlassList'

export default class ExGlassMapping extends Component {
    state = {
        glassArray: dataGlass,
        glassMapping: dataGlass[8],

    };
    handleGlassMapping=(data)=>{
      this.setState({glassMapping: data});
    }
    
  render() {
    return (
      <div className="content">
        <div className="title">TRY GLASSES APP ONLINE</div>
        {/* glass detail/mapping */}
        <GlassDetail mappingDetail={this.state.glassMapping}></GlassDetail>
        {/* glass list */}
        <GlassList mappingFunction={this.handleGlassMapping} glassArray={this.state.glassArray}></GlassList>
      </div>
    )
  }
}
