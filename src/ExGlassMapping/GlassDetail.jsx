import React, { Component } from 'react'

export default class GlassDetail extends Component {
  render() {
    let {url,name,price,desc} = this.props.mappingDetail;
    return (
      <div>
        {/* content here */}
        <div className="row">
                <div className="col-3">
                </div>
                <div className="col-3">
                    <div className="vglasses__card">
                        <div className="vglasses__model" id="avatar">
                            {/* virtual glass here */}
                            <div id="mappingToogle">
                                <img src="./glassesImage/v1.png" alt='' />
                            </div>
                            {/* end */}
                        </div>
                        <div id="glassesInfoLeft" className="vglasses__info_left">
                            {/* glass info here */}
                            <div id="mappingToogleInfo">
                                <p>GUCCI G8850U </p>
                                <p className="price">$30<span>Stocking</span></p>
                                <p>Light pink square lenses define these sunglasses, ending with amother of pearl effect tip.</p>
                            </div>
                            {/* end */}
                        </div>
                    </div>
                </div>
                <div className="col-3">
                    <div className="vglasses__card">
                        <div className="vglasses__model" id="avatar">
                            {/* virtual glass here */}
                            <div id="mappingToogle">
                                <img src={url} alt='' />
                            </div>
                            {/* end */}
                        </div>
                        <div id="glassesInfoRight" className="vglasses__info_right">
                            {/* glass info here */}
                            <div id="mappingToogleInfo">
                                <p>{name}</p>
                                <p className="price">${price}<span>Stocking</span></p>
                                <p>{desc}</p>
                            </div>
                            {/* end */}
                        </div>
                    </div>
                </div>
                <div className="col-3">
                </div>
            </div>
      </div>
    )
  }
}
