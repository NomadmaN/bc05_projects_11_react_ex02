import React, { Component } from 'react'

export default class GlassItem extends Component {
  render() {
    let {id,url} = this.props.data;
    return (
        <div id={id} className="vglasses__items col-2 my-2">
            <img onClick={()=>{this.props.mappingGlass(this.props.data)}} src={url} alt='' title="Click to map this glass on model" />
        </div>
    )
  }
}