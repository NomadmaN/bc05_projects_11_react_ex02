import React, { Component } from 'react'
import GlassItem from './GlassItem'

export default class GlassList extends Component {
    renderGlassList =()=>{
        return this.props.glassArray.map ((item) => {
            return <GlassItem mappingGlass={this.props.mappingFunction} data={item}></GlassItem>})
    }
  render() {
    
    return (
      <div>
        {/* content here */}
        <div className="container mx-auto">
                <div className="glasses__list row p-2">
                    {this.renderGlassList()}
                </div>
            </div>
      </div>
    )
  }
}
